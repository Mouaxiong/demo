## Run Project

Follow the instructions below to run the application.

1. Clone the application by checking out the repo locally.
2. Ensure you have node version 10 or greater installed.
3. Create a local folder where you will check out the application to.
4. Open up a command line or VS Code and run npm install.  Type **npm install** in the directory.
5. Install angular cli if you don't already have it installed.  Type **npm install -g @angular/cli**.
5. After npm runs, you can then start up the application.  Type **ng serve**.