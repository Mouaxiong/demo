import { Cart } from './../models/Cart/cart.model';
import { Component, OnInit } from '@angular/core';
import { CartService } from '../providers/cart/cart.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {
  public cart = new Cart();
  constructor(private cartService: CartService) { }

  ngOnInit() {
    this.cartService.getCart().subscribe(cart => {
      this.cart = cart;
    });
  }

  clearCart() {
    this.cartService.emptyCart();
  }
}
