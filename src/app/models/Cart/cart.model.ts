import { CartItem } from './cartItem.model';
import { Product } from "../Product/product.model";

export class Cart {   
    constructor() {
        this.cartTotal = 0;
        this.salesTax = 0;
        this.cartItems = [];
    }   
    cartItems: CartItem[];
    cartTotal: number;
    salesTax: number;
}