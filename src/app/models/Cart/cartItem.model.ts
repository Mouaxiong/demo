import { Product } from "../Product/product.model";

export class CartItem {    
    isSuccess: boolean;    
    product: Product;
    quantity: number;
    itemTotal: number;
    itemTax: number;
}