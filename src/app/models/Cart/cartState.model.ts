import { Product } from "../Product/product.model";

export class CartState {   
    constructor() {}   
    quantity: 0;
    product: Product;
    isClearCart: boolean;
}