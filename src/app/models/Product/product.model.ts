
export class Product {
    id: number;
    name: string;
    price: number;
    taxExempt: false;
    isImported: false;
}