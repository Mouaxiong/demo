import { Component, OnInit } from '@angular/core';
import { Input } from '@angular/core';
import { Output, EventEmitter } from '@angular/core';


@Component({
  selector: 'app-notify-alert',
  templateUrl: './notify-alert.component.html',
  styleUrls: ['./notify-alert.component.css']
})
export class NotifyAlertComponent implements OnInit {
  @Input() product;
  @Output() notify = new EventEmitter();
  constructor() {}
  
  ngOnInit() {
  }
}


/*
Copyright Google LLC. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at https://angular.io/license
*/