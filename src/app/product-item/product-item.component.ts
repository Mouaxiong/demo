import { Component, OnInit } from '@angular/core';
import { Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Output, EventEmitter } from '@angular/core';
import { CartService } from '../providers/cart/cart.service';

@Component({
  selector: 'app-product-item',
  templateUrl: './product-item.component.html',
  styleUrls: ['./product-item.component.css']
})
export class ProductItemComponent implements OnInit {
  @Input() product;
  @Output() notifyAdded = new EventEmitter();
  public formControlGroup: FormGroup;
  public quantity: number;
  public submitted: boolean;
  public addedToCart: boolean;
  constructor(private cartService: CartService, private formBuilder: FormBuilder) {}
  
  ngOnInit() {
    this.formControlGroup = this.formBuilder.group({
      quantity: [this.quantity, [Validators.required, Validators.pattern(/^[0-9]\d*$/), Validators.min(1)]]
    });

    this.submitted = false;
  }

  onQuantityChange(event : any) {
    // Clear out added to cart.
    this.addedToCart = false;
  }

  addToCart(product) {
    this.submitted = true;
    this.addedToCart = false;
    if (this.formControlGroup.valid) {
        this.cartService.addItemToCart(product, this.formControlGroup.controls.quantity.value).subscribe(prod => {
          this.formControlGroup.controls.quantity.reset();
          this.addedToCart = true;
        }) ;
    } 
  }
}
