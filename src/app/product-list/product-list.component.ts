import { Component, OnInit } from '@angular/core';

import { ProductService } from '../providers/product/product.service';
import { CartService } from '../providers/cart/cart.service';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit {
  public products: any;
  constructor(private productService: ProductService, 
    private cartService: CartService) {}

  getProducts() {
    this.productService.getProducts().subscribe(products => {
      this.products = products
    });
  }

  ngOnInit() {
    this.getProducts();
  }

}


/*
Copyright Google LLC. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at https://angular.io/license
*/