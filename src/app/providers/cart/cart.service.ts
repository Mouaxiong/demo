import { CartItem } from './../../models/Cart/cartItem.model';
import { Cart } from './../../models/Cart/cart.model';
import { CartState } from './../../models/Cart/cartState.model';
import { Injectable } from '@angular/core';
import { Observable, of, Subject } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CartService {
  private cart = new Cart();
  private cartSubject = new Subject<CartState>();
  CartState = this.cartSubject.asObservable();

  constructor(private http: HttpClient) { }

  addItemToCart(product, quantity) : Observable<any>{
    
    let cartItm = new CartItem();
    cartItm.product = product;
    let qty = parseInt(quantity);
    cartItm.quantity = qty;

    // See if the item is already added to the cart.
    let existingCartItm = this.cart.cartItems.find(itm => itm.product.id == product.id);

    if (existingCartItm) {
      existingCartItm.quantity += qty;
      cartItm = existingCartItm;
    }

    this.calculateItemPrice(cartItm).subscribe(itm => {
      itm.isSuccess = true;
      this.cartSubject.next(<CartState>{ quantity: quantity, product: product });
      if (!existingCartItm) {
        this.cart.cartItems.push(itm);
      }
      this.calculateCartTotal();
    });
    return of(cartItm);
  }

  emptyCart() : Observable<any>{
    this.cart.cartItems = [];
    this.cartSubject.next(<CartState>{ isClearCart: true, quantity: null, product: null });
    return of(this.cart.cartItems);
  }

  calculateItemPrice(cartItem) : Observable<any>{
    let itemTax = 0;
    let baseTotal = (cartItem.product.price * parseInt(cartItem.quantity));
  
    if (!cartItem.product.taxExempt) {
      // Calculate price with tax of .1 
      itemTax = .1 * baseTotal;
    }
    let taxTotal = cartItem.product.price * cartItem.quantity + itemTax;

    if (cartItem.product.isImported) {
      let importedTax = baseTotal * .05;
      cartItem.itemTax =(Math.ceil((itemTax + importedTax)*20)/20);
    } else {
      cartItem.itemTax = (Math.ceil((itemTax)*20)/20);
    }

    cartItem.itemTotal = baseTotal;

    return of(cartItem);
  }

  calculateCartTotal() : Observable<Cart> {
    this.cart.cartTotal = 0;
    this.cart.salesTax = 0;
    this.cart.cartItems.forEach(itm => {
      this.cart.cartTotal += itm.itemTax + itm.itemTotal;
      this.cart.salesTax += itm.itemTax;
    });
    return of(this.cart);
  }

  getCart() : Observable<any> {
    return of (this.cart);
  }

  ngOnDestroy() {
    this.cartSubject.next();
    // Complete the notifying Observable to remove it
    this.cartSubject.complete();
  }
}