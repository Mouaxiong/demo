import { TestBed, inject } from '@angular/core/testing';
import { Observable } from 'rxjs';

import { ProductService } from './product.service';

describe('ProductService', () => {
  let service: ProductService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ProductService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('#getProducts', () => {
    it('creates the getProducts() method', inject([ProductService], (productService: ProductService) => {
      expect(productService.getProducts).toBeDefined();
    }));

    it('get list of products', inject([ProductService], (productService: ProductService) => {      
      expect(productService.getProducts()).toEqual(jasmine.any(Observable));
    }));
  });
});
