import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/internal/Subscription';
import { CartService } from '../providers/cart/cart.service';
import { CartState } from './../models/Cart/cartState.model';

@Component({
  selector: 'app-top-bar',
  templateUrl: './top-bar.component.html',
  styleUrls: ['./top-bar.component.css']
})
export class TopBarComponent {
  private subscription : Subscription;
  public totalCartItems : number = 0;
  constructor(private route: ActivatedRoute, private cartService: CartService) {
    this.subscription = this.cartService.CartState
    .subscribe((state : CartState) => {
      if (state.isClearCart) {
        this.totalCartItems = 0;
      } else if (state.quantity) {
        this.totalCartItems += parseInt(state.quantity);
      }  
    });
  }
}
